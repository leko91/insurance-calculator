import { Outlet, useLocation } from 'react-router-dom';
import { Center, Icon, useColorModeValue } from '@chakra-ui/react';
import { AiOutlineCalculator } from 'react-icons/ai';

import { InfoBox, Layout } from './components';
import { LOREM_IPSUM } from './utils';

export const App = () => {
  const location = useLocation();

  const backgroundImageShadowDisplay = useColorModeValue('none', 'block');

  return (
    <Layout>
      {location.pathname === '/' ? (
        <Center
          flexDir="column"
          h="calc(100vh - 150px)"
          px={{ base: 8, md: 0 }}
          py="100px"
          backgroundImage="url('images/hero.jpg')"
          backgroundSize="cover"
          _before={{
            position: 'absolute',
            top: { base: 0, md: '100px' },
            left: 0,
            display: backgroundImageShadowDisplay,
            width: '100%',
            height: { base: 'calc(100% - 150px)', md: 'calc(100% - 250px)' },
            content: '""',
            backgroundColor: 'rgba(0, 0, 0, .6)',
          }}
        >
          <InfoBox contentText={LOREM_IPSUM} linkHref="/calculator">
            Go to best calculator{' '}
            <Icon as={AiOutlineCalculator} fontSize={{ base: 32, md: 24 }} />
          </InfoBox>
        </Center>
      ) : null}

      <Outlet />
    </Layout>
  );
};
