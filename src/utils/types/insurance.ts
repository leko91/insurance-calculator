export interface QuoteData {
  name: string;
  age: number;
  gender: 'male' | 'female' | 'undisclosed';
  yearlyDistance: number[];
  country: string;
  betterPrice: boolean;
  quote?: number;
}
