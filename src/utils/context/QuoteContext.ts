import { createContext, useContext } from 'react';

import { QuoteData } from '../types';

type QuoteContextData = {
  quoteData: Nullable<QuoteData>;
};

export const QuoteContext = createContext<QuoteContextData>(
  {} as QuoteContextData
);

export const useQuoteContext = () => {
  const context = useContext(QuoteContext);

  if (context === undefined) {
    throw new Error('useQuoteContext must be within a Provider');
  }

  return context;
};
