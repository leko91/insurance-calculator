import { RANDOM_X, RANDOM_Y } from '../constants';
import { QuoteData } from '../types';

export const calculateInsurance = (inputData: QuoteData): number => {
  const BASE = 10000;
  let quote: number;

  // Age
  quote = (BASE * inputData.age) / 2;

  // Gender
  quote += 1000000;
  switch (inputData.gender) {
    case 'male':
      quote *= RANDOM_X * RANDOM_X;
      break;
    case 'female':
      quote *= RANDOM_X * RANDOM_Y;
      break;
    case 'undisclosed':
      quote *= RANDOM_Y * RANDOM_Y;
      break;
    default:
      break;
  }

  // Yearly distance
  quote += inputData.yearlyDistance[0] + inputData.yearlyDistance[1];

  // Country
  quote += inputData.country.length * 1000;

  // Better quote
  if (inputData.betterPrice) {
    quote /= 2;
  }

  return Math.round(quote);
};

export const getPreviousQuotes = () => {
  const storedData = { ...localStorage };

  const storedQuotesObject = Object.fromEntries(
    Object.entries(storedData).filter(([key]) => key.includes('insurance:'))
  );

  const storedQuotes = Object.keys(storedQuotesObject).map((key) =>
    JSON.parse(storedQuotesObject[key])
  );

  return storedQuotes;
};
