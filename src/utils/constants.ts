export const COUNTRIES_API_URL = 'https://restcountries.com/v3.1/';

export const RANDOM_X = Math.random();
export const RANDOM_Y = Math.random();

export const LOREM_IPSUM =
  'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Enim voluptatibus odit cumque eligendi aliquam veritatis, ipsum debitis perspiciatis quia officiis sunt animi maiores amet possimus beata mollitia illum, accusamus corrupti.';
