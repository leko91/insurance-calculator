import axios from 'axios';
import { useEffect, useState } from 'react';

import { COUNTRIES_API_URL } from '../constants';

export interface Country {
  name: {
    common: string;
    nativeName: Record<string, unknown>[];
    official: string;
  };
}

export const useCountries = () => {
  const [countries, setCountries] = useState<Country[]>([]);

  const getCountries = async () => {
    await axios
      .get<Country[]>(`${COUNTRIES_API_URL}/all?fields=name`)
      .then((res) => {
        const sortedCountries = res.data.sort((a, b) =>
          a.name.common.localeCompare(b.name.common)
        );

        setCountries(sortedCountries);
      })
      .catch((error) => {
        throw new Error(`There has been an error: ${error}`);
      });
  };

  useEffect(() => {
    getCountries();
  }, []);

  return { countries };
};
