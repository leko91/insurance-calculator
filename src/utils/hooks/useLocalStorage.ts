import { useEffect, useRef, useState } from 'react';

export const useLocalStorageState = (
  key: string,
  defaultValue: string | (() => string) = '',
  { serialize = JSON.stringify, deserialize = JSON.parse } = {}
) => {
  const [state, setState] = useState(() => {
    const valueInLocalStorage = window.localStorage.getItem(key);

    if (valueInLocalStorage) {
      return deserialize(valueInLocalStorage);
    }

    return typeof defaultValue === 'function' ? defaultValue() : defaultValue;
  });

  const previousKeyRef = useRef(key);

  useEffect(() => {
    previousKeyRef.current = key;

    if (state) {
      window.localStorage.setItem(key, serialize(state));
    }
  }, [key, state, serialize]);

  return [state, setState];
};
