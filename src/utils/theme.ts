import { extendTheme } from '@chakra-ui/react';
import { mode } from '@chakra-ui/theme-tools';
import type { StyleFunctionProps } from '@chakra-ui/styled-system';

const fonts = {
  body: `'Alegreya SC', serif`,
};

const colors = {
  brand: {
    primary: '#41907A',
    secondary: '#6575A8',
  },
  default: {
    light: '#91b0d8',
    dark: '#475d76',
  },
  red: {
    300: '#9b2c2c',
  },
};

const styles = {
  global: (props: StyleFunctionProps) => ({
    body: {
      color: mode('gray.800', 'whiteAlpha.900')(props),
      bg: mode('default.light', 'default.dark')(props),
    },
  }),
};

const components = {
  Link: {
    variants: {
      button: {
        display: 'inline-flex',
        alignItems: 'center',
        gap: 2,
        p: 5,
        fontSize: '2xl',
        color: '#FFF',
        backgroundColor: 'brand.primary',
        borderWidth: '1px',
        borderRadius: 'lg',
        transition: 'filter .3s',
        _hover: {
          textDecoration: 'none',
          filter: 'brightness(85%)',
        },
      },
    },
  },
  Button: {
    baseStyle: {
      transition: 'filter .3s',
      _hover: {
        textDecoration: 'none',
        filter: 'brightness(85%)',
        _disabled: {
          backgroundColor: 'inherit',
          filter: 'none',
        },
      },
    },
    variants: {
      primary: {
        display: 'block',
        height: '50px',
        marginLeft: 'auto',
        padding: '15px 30px',
        fontSize: '18px',
        color: '#FFF',
        textTransform: 'uppercase',
        backgroundColor: 'brand.primary',
      },
      secondary: {
        display: 'block',
        height: '40px',
        padding: '12px 24px',
        fontSize: '16px',
        color: '#FFF',
        textTransform: 'uppercase',
        backgroundColor: 'brand.secondary',
        _hover: {
          _disabled: {
            backgroundColor: 'brand.secondary',
            filter: 'none',
          },
        },
      },
    },
  },
  Form: {
    baseStyle: {
      container: {
        marginBottom: 4,
        // _last: {
        //   margin: 0,
        // },
      },
    },
  },
  FormLabel: {
    baseStyle: {
      cursor: 'pointer',
    },
  },
  Drawer: {
    baseStyle: (props: StyleFunctionProps) => ({
      dialog: {
        bg: mode('#FFF', 'default.dark')(props),
      },
    }),
  },
};

export const chakraThemeConfig = extendTheme({
  initialColorMode: 'light',
  useSystemColorMode: false,
  fonts,
  colors,
  styles,
  components,
});
