import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { ChakraProvider, ColorModeScript } from '@chakra-ui/react';

import { App } from './App';
import { InsuranceCalculator, QuotesList } from './components';
import { chakraThemeConfig } from './utils';
import ErrorPage from './errorPage';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: '/calculator',
        element: <InsuranceCalculator />,
      },
      {
        path: '/quotes',
        element: <QuotesList />,
      },
    ],
  },
]);

const container = document.getElementById('root');
if (!container) throw new Error('Failed to find the root element');
const root = ReactDOM.createRoot(container);

root.render(
  <React.StrictMode>
    <ChakraProvider theme={chakraThemeConfig}>
      <RouterProvider router={router} />
      <ColorModeScript />
    </ChakraProvider>
  </React.StrictMode>
);
