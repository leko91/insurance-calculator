import { Box } from '@chakra-ui/react';

import { Header } from './Header';
import { Footer } from './Footer';

interface LayoutProps {
  children: React.ReactNode;
}

export const Layout = ({ children }: LayoutProps) => {
  return (
    <Box>
      <Header />

      <Box>{children}</Box>

      <Footer />
    </Box>
  );
};
