import { useRef } from 'react';
import {
  useColorModeValue,
  Flex,
  Image,
  Link,
  Button,
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  useDisclosure,
} from '@chakra-ui/react';
import classnames from 'classnames';

import { ColorModeSwitcher } from '../../ColorModeSwitcher';
import styles from './Header.module.css';

export const Header = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = useRef<HTMLButtonElement>(null);

  const headerBackgroundColor = useColorModeValue('#FFF', 'default.dark');

  return (
    <>
      <Flex
        as="header"
        position="fixed"
        top="0"
        left="0"
        zIndex={4}
        justifyContent="space-between"
        alignItems="center"
        display={{
          base: 'none',
          md: 'flex',
        }}
        w="100%"
        h={{ base: 'auto', md: '100px' }}
        p={5}
        backgroundColor={headerBackgroundColor}
        boxShadow="2xl"
      >
        <Flex>
          <Link href="/">
            <Image src="images/logo.svg" w="60px" h="60px" />
          </Link>
        </Flex>

        <ColorModeSwitcher justifySelf="flex-end" />
      </Flex>

      <Button
        ref={btnRef}
        className={classnames(styles.drawerButton, {
          open: isOpen,
        })}
        display={{
          base: 'flex',
          md: 'none',
        }}
        variant="ghost"
        onClick={() => {
          if (!isOpen) {
            onOpen();
          } else {
            onClose();
          }
        }}
      >
        <span></span>
        <span></span>
        <span></span>
      </Button>

      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent justifyContent="space-between">
          <DrawerHeader display="flex" justifyContent="flex-end">
            <Image src="images/logo.svg" w="48px" h="48px" />
          </DrawerHeader>

          <DrawerBody py={5}>
            <Link href="/">Home</Link>
          </DrawerBody>

          <DrawerFooter>
            <ColorModeSwitcher justifySelf="flex-end" />
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
};
