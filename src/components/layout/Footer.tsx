import { Box, Center, Image, Text } from '@chakra-ui/react';

export const Footer = () => {
  return (
    <Box
      as="footer"
      h="150px"
      p={5}
      backgroundColor="brand.primary"
      boxShadow="0 -15px 50px -12px rgba(0, 0, 0, 0.25)"
    >
      <Center flexDir="column">
        <Image src="images/logo-white.svg" w="80px" h="80px" mb={3} />

        <Text as="span" fontSize="xs" color="#FFF">
          Copyright &copy; {new Date().getFullYear()} All rights reserved.
        </Text>
      </Center>
    </Box>
  );
};
