import { Container, Link, Text } from '@chakra-ui/react';

interface InfoBoxProps {
  contentText: string;
  linkHref: string;
  onClick?: () => void;

  children: React.ReactNode;
}

export const InfoBox = ({
  contentText,
  linkHref,
  onClick,
  children,
}: InfoBoxProps) => {
  return (
    <Container
      position="relative"
      zIndex={1}
      p={{
        base: 3,
        md: 5,
      }}
      backgroundColor="brand.secondary"
      borderWidth="1px"
      borderRadius="lg"
      textAlign="center"
    >
      <Text mb={5} color="#FFF">
        {contentText}
      </Text>

      <Link
        href={linkHref}
        variant="button"
        p={{
          base: 3,
          md: 5,
        }}
        onClick={onClick}
      >
        {children}
      </Link>
    </Container>
  );
};
