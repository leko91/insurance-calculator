import { useMemo, useState } from 'react';
import {
  Checkbox,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/react';

import { QuoteForm } from '../forms';
import { QuoteData } from '../../../utils';

interface QuoteEditModalProps {
  editModal: Nullable<string>;
  allQuotesData: QuoteData[];
  onClose: () => void;
}

export const QuoteEditModal = ({
  editModal,
  allQuotesData,
  onClose,
}: QuoteEditModalProps) => {
  const [isLiveEdit, setIsLiveEdit] = useState<boolean>(false);

  const selectedQuoteData = useMemo(
    () => allQuotesData.filter((quote) => quote.name === editModal)[0],
    [allQuotesData, editModal]
  );

  return (
    <Modal
      blockScrollOnMount={false}
      isOpen={!!editModal}
      onClose={() => {
        onClose();
      }}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Edit {editModal}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Checkbox
            mb={5}
            onChange={(event) => setIsLiveEdit(event.target.checked)}
          >
            I want to see the changes live
          </Checkbox>

          <QuoteForm quoteData={selectedQuoteData} liveEdit={isLiveEdit} />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
