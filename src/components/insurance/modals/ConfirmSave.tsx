import { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/react';

import { useQuoteContext } from '../../../utils';

const VALIDATION_SCHEMA = yup.object({
  name: yup
    .string()
    .required('Name cannot be empty.')
    .test(
      'name',
      'That name also exists, please save under a new one.',
      (value) => !window.localStorage.getItem(`insurance: ${value}`)
    ),
});

interface FormData {
  name: string;
}

interface ConfirmSaveModalProps {
  isOpen: boolean;
  onClose: () => void;
  onNameChangeSave: (name: string) => void;
  onOverwriteSave: () => void;
}

export const ConfirmSaveModal = ({
  isOpen,
  onClose,
  onNameChangeSave,
  onOverwriteSave,
}: ConfirmSaveModalProps) => {
  const { quoteData } = useQuoteContext();

  const [isNameChange, setNameChange] = useState<boolean>(false);

  const {
    control,
    formState: { errors },
    handleSubmit,
  } = useForm<FormData>({
    defaultValues: {
      name: quoteData?.name,
    },
    resolver: yupResolver(VALIDATION_SCHEMA),
  });

  const onSubmit = (data: FormData) => {
    onNameChangeSave(data.name);
  };

  const handleNameChange = () => {
    setNameChange(true);
  };

  const handleNameChangeCancel = () => {
    setNameChange(false);
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Confirm save</ModalHeader>
        <ModalCloseButton />
        {isNameChange ? (
          <form onSubmit={handleSubmit(onSubmit)}>
            <ModalBody>
              <FormControl isInvalid={!!errors.name}>
                <FormLabel htmlFor="name">New name</FormLabel>

                <Controller
                  name="name"
                  control={control}
                  render={({ field }) => <Input {...field} id="name" />}
                />

                {errors.name ? (
                  <FormErrorMessage>{errors.name.message}</FormErrorMessage>
                ) : (
                  <FormHelperText>
                    Please choose a different name for your quote.
                  </FormHelperText>
                )}
              </FormControl>
            </ModalBody>

            <ModalFooter gap={3}>
              <Button variant="secondary" onClick={handleNameChangeCancel}>
                Cancel
              </Button>

              <Button type="submit" variant="primary" ml="auto">
                Save
              </Button>
            </ModalFooter>
          </form>
        ) : (
          <>
            <ModalBody>
              <span>
                You already have a quote saved under this name, do you wish to
                save it under a different name or overwrite the existing one?
              </span>
            </ModalBody>

            <ModalFooter gap={3}>
              <Button
                variant="secondary"
                fontSize="12px"
                onClick={handleNameChange}
              >
                Save under a different name
              </Button>

              <Button variant="primary" onClick={onOverwriteSave}>
                Overwrite
              </Button>
            </ModalFooter>
          </>
        )}
      </ModalContent>
    </Modal>
  );
};
