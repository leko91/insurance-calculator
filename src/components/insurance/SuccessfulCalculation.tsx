import { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Flex,
  Link,
  ListItem,
  Spinner,
  Text,
  Tooltip,
  UnorderedList,
  useDisclosure,
} from '@chakra-ui/react';
import CountUp from 'react-countup';

import { QuoteData, QuoteContext, useLocalStorageState } from '../../utils';
import { ConfirmSaveModal } from './modals';

interface SuccessfulCalculationProps {
  quoteData: QuoteData;
  quote: number;
  onNewQuote: () => void;
}

export const SuccessfulCalculation = ({
  quoteData,
  quote,
  onNewQuote,
}: SuccessfulCalculationProps) => {
  const [isCalculated, setIsCalculated] = useState<boolean>(false);
  const [isRecentlySaved, setIsRecentlySaved] = useState<boolean>(false);
  const [insuranceData, setInsuranceData] = useState<QuoteData>(quoteData);

  const {
    isOpen,
    onOpen: onSaveConfirmationOpen,
    onClose: onSaveConfirmationClose,
  } = useDisclosure();

  const [localQuoteData, setLocalQuoteData] = useLocalStorageState(
    `insurance: ${insuranceData.name}`
  );

  useEffect(() => {
    const calculationTimeout = setTimeout(() => {
      setIsCalculated(true);
    }, 2000);

    return () => clearTimeout(calculationTimeout);
  }, []);

  const handleSave = () => {
    if (!localQuoteData) {
      setLocalQuoteData({ ...insuranceData, quote: quote });
      setIsRecentlySaved(true);
    } else {
      onSaveConfirmationOpen();
    }
  };

  const handleNameChangeSave = (name: string) => {
    setInsuranceData((previousState) => {
      const newInsuranceData = { ...previousState, name };

      setLocalQuoteData(newInsuranceData);

      return newInsuranceData;
    });
    setIsRecentlySaved(true);
    onSaveConfirmationClose();
  };

  const handleOverwriteSave = () => {
    setLocalQuoteData(insuranceData);
    setIsRecentlySaved(true);
    onSaveConfirmationClose();
  };

  if (isCalculated) {
    return (
      <QuoteContext.Provider value={{ quoteData: insuranceData }}>
        <UnorderedList fontSize="xl">
          <ListItem>Name: {insuranceData.name}</ListItem>
          <ListItem>Age: {insuranceData.age}</ListItem>
          <ListItem>Gender: {insuranceData.gender}</ListItem>
          <ListItem>Country: {insuranceData.country}</ListItem>
          <ListItem>
            Yearly distance: {insuranceData.yearlyDistance[0]}km -{' '}
            {insuranceData.yearlyDistance[1]}km
          </ListItem>
        </UnorderedList>

        <Text mb={5} fontSize="5xl" textAlign="center">
          ${quote}
        </Text>

        <Flex justifyContent="space-between">
          <Button
            variant="secondary"
            onClick={handleSave}
            isDisabled={isRecentlySaved}
          >
            {isRecentlySaved ? 'Saved' : 'Save'}
          </Button>

          <Button variant="secondary" isDisabled={!isRecentlySaved}>
            <Tooltip
              label="You need to save you quote to be able to compare it"
              placement="top"
              aria-label="Compare quotes"
              isDisabled={isRecentlySaved}
            >
              <Link
                href="/quotes"
                pointerEvents={!isRecentlySaved ? 'none' : 'auto'}
                _hover={{
                  textDecoration: 'none',
                }}
              >
                Compare
              </Link>
            </Tooltip>
          </Button>

          <Button variant="secondary" onClick={onNewQuote}>
            New quote
          </Button>
        </Flex>

        <ConfirmSaveModal
          isOpen={isOpen}
          onClose={onSaveConfirmationClose}
          onNameChangeSave={handleNameChangeSave}
          onOverwriteSave={handleOverwriteSave}
        />
      </QuoteContext.Provider>
    );
  }

  return (
    <Flex flexDir="column" justifyContent="center" alignItems="center" gap={10}>
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="brand.primary"
        size="xl"
      />

      <Text fontSize="5xl">
        $<CountUp end={quote} />
      </Text>
    </Flex>
  );
};
