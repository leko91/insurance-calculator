import { useRef, useState } from 'react';
import {
  Box,
  Center,
  Container,
  Text,
  useColorModeValue,
} from '@chakra-ui/react';

import { QuoteForm } from './forms';
import { InfoBox } from '../layout';
import { LOREM_IPSUM } from '../../utils';

export const InsuranceCalculator = () => {
  const [isNewQuote, setIsNewQuote] = useState<boolean>(false);

  const calculatorRef = useRef<HTMLDivElement>(null);

  const calculatorBackgroundColor = useColorModeValue('#fff', 'default.light');

  const handleGetQuote = () => {
    calculatorRef.current?.scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <>
      {!isNewQuote ? (
        <Center
          minH={{ base: '100vh', md: 'calc(100vh - 100px)' }}
          px={{ base: 8, md: 0 }}
        >
          <InfoBox
            contentText={LOREM_IPSUM}
            linkHref="#"
            onClick={handleGetQuote}
          >
            Get your quote now!
          </InfoBox>
        </Center>
      ) : null}

      <Center ref={calculatorRef} flexDir="column" minH="100vh" py={16}>
        <Container textAlign="center">
          <Text mb={10} fontSize="5xl">
            Quote Calculator
          </Text>

          <Box
            p={8}
            textAlign="left"
            backgroundColor={calculatorBackgroundColor}
            borderWidth="1px"
            borderRadius="lg"
          >
            <QuoteForm
              onNewQuote={() => {
                setIsNewQuote(true);
              }}
            />
          </Box>
        </Container>
      </Center>
    </>
  );
};
