import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  FormLabel,
  Input,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Radio,
  RadioGroup,
  RangeSlider,
  RangeSliderFilledTrack,
  RangeSliderThumb,
  RangeSliderTrack,
  Select,
  Spinner,
  Stack,
  Switch,
  Text,
} from '@chakra-ui/react';

import { SuccessfulCalculation } from '../SuccessfulCalculation';
import {
  QuoteData,
  calculateInsurance,
  useCountries,
  useLocalStorageState,
} from '../../../utils';

const VALIDATION_SCHEMA = yup.object({
  name: yup.string().required('This field is required.'),
  age: yup
    .number()
    .typeError('This field is required.')
    .required('This field is required.')
    .min(18, 'You must be more than 18 years old.')
    .max(122, 'You cannot be older than the oldest person ever.'),
  gender: yup
    .string()
    .oneOf(['male', 'female', 'undisclosed'])
    .required('Please select a gender.'),
  yearlyDistance: yup
    .array()
    .of(yup.number().required())
    .required()
    .length(2)
    .test(
      'yearlyDistance',
      'Please provide a more precise value.',
      (value) => Math.abs(value[0] - value[1]) < 15000
    ),
  country: yup.string().required('Please select a country.'),
  betterPrice: yup.boolean().required(),
});

type FormData = {
  name: string;
  age: number;
  gender: 'male' | 'female' | 'undisclosed';
  yearlyDistance: number[];
  country: string;
  betterPrice: boolean;
};

interface QuoteFormProps {
  quoteData?: QuoteData;
  onNewQuote?: () => void;
  liveEdit?: boolean;
}

export const QuoteForm = ({
  quoteData,
  onNewQuote,
  liveEdit,
}: QuoteFormProps) => {
  const [quote, setQuote] = useState<number>(0);

  const { countries } = useCountries();

  const [localQuoteData, setLocalQuoteData] = useLocalStorageState(
    `insurance: ${quoteData?.name}`
  );

  const {
    control,
    formState: { isSubmitting, isSubmitSuccessful, errors },
    getValues,
    handleSubmit,
    reset,
    watch,
  } = useForm<FormData>({
    defaultValues: {
      name: quoteData?.name ?? '',
      age: quoteData?.age ?? undefined,
      gender: quoteData?.gender ?? undefined,
      yearlyDistance: quoteData?.yearlyDistance ?? [1000, 100000],
      country: quoteData?.country ?? '',
      betterPrice: quoteData?.betterPrice ?? false,
    },
    resolver: yupResolver(VALIDATION_SCHEMA),
  });

  const watchAllFields = watch();

  useEffect(() => {
    if (liveEdit) {
      setQuote(calculateInsurance(getValues()));
    }
  }, [getValues, liveEdit, watchAllFields]);

  const onSubmit = (data: FormData) => {
    setQuote(calculateInsurance(data));

    if (quoteData) {
      setLocalQuoteData({ ...localQuoteData, ...data, quote: quote });
    }
  };

  const yearlyDistance = watch('yearlyDistance');

  if (!quoteData && isSubmitting) {
    return <Spinner />;
  }

  if (!quoteData && isSubmitSuccessful && quote > 0) {
    return (
      <SuccessfulCalculation
        quoteData={{
          name: getValues('name'),
          age: getValues('age'),
          gender: getValues('gender'),
          country: getValues('country'),
          yearlyDistance: getValues('yearlyDistance'),
          betterPrice: getValues('betterPrice'),
        }}
        quote={quote}
        onNewQuote={() => {
          reset();
          setQuote(0);
          onNewQuote?.();
        }}
      />
    );
  }

  return (
    <>
      {liveEdit ? (
        <Text mb={5} fontSize="2xl">
          Quote: ${quote}
        </Text>
      ) : null}

      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl isInvalid={!!errors.name}>
          <FormLabel htmlFor="name">Name</FormLabel>

          <Controller
            name="name"
            control={control}
            render={({ field }) => (
              <Input {...field} id="name" isDisabled={!!quoteData} />
            )}
          />

          {errors.name ? (
            <FormErrorMessage>{errors.name.message}</FormErrorMessage>
          ) : (
            <FormHelperText>Name of the quote, not your name.</FormHelperText>
          )}
        </FormControl>

        <FormControl isInvalid={!!errors.age}>
          <FormLabel htmlFor="age">Age</FormLabel>

          <Controller
            name="age"
            control={control}
            render={({ field }) => (
              <NumberInput
                {...field}
                id="age"
                min={18}
                max={122}
                keepWithinRange={false}
                clampValueOnBlur={false}
                allowMouseWheel
              >
                <NumberInputField />
                <NumberInputStepper>
                  <NumberIncrementStepper />
                  <NumberDecrementStepper />
                </NumberInputStepper>
              </NumberInput>
            )}
          />

          {errors.age ? (
            <FormErrorMessage>{errors.age.message}</FormErrorMessage>
          ) : null}
        </FormControl>

        <FormControl isInvalid={!!errors.gender}>
          <FormLabel>Gender</FormLabel>

          <Controller
            name="gender"
            control={control}
            render={({ field }) => (
              <RadioGroup {...field}>
                <Stack>
                  <Radio value="male">Male</Radio>
                  <Radio value="female">Female</Radio>
                  <Radio value="undisclosed">Prefer not to answer</Radio>
                </Stack>
              </RadioGroup>
            )}
          />

          {errors.gender ? (
            <FormErrorMessage>{errors.gender.message}</FormErrorMessage>
          ) : null}
        </FormControl>

        <FormControl isInvalid={!!errors.yearlyDistance}>
          <FormLabel>How many kilometers do you drive in a year?</FormLabel>

          <Controller
            name="yearlyDistance"
            control={control}
            render={({ field }) => (
              <RangeSlider
                {...field}
                // eslint-disable-next-line jsx-a11y/aria-proptypes
                aria-label={['min', 'max']}
                min={1000}
                max={100000}
                step={1000}
                minStepsBetweenThumbs={2}
                defaultValue={[1000, 100000]}
              >
                <RangeSliderTrack>
                  <RangeSliderFilledTrack />
                </RangeSliderTrack>
                <RangeSliderThumb index={0} />
                <RangeSliderThumb index={1} />
              </RangeSlider>
            )}
          />

          <Text>
            {yearlyDistance[0]}km - {yearlyDistance[1]}km
          </Text>

          {errors.yearlyDistance ? (
            <FormErrorMessage>{errors.yearlyDistance.message}</FormErrorMessage>
          ) : null}
        </FormControl>

        <FormControl isInvalid={!!errors.country}>
          <FormLabel>Country</FormLabel>

          <Controller
            name="country"
            control={control}
            render={({ field }) => (
              <Select {...field} placeholder="Select country">
                {countries.map((country) => (
                  <option
                    key={country.name.official}
                    value={country.name.common}
                  >
                    {country.name.common}
                  </option>
                ))}
              </Select>
            )}
          />

          {errors.country ? (
            <FormErrorMessage>{errors.country.message}</FormErrorMessage>
          ) : null}
        </FormControl>

        <FormControl display="flex" alignItems="center" gap={2}>
          <FormLabel htmlFor="betterPrice" m={0}>
            I wish to get a better price.
          </FormLabel>

          <Controller
            name="betterPrice"
            control={control}
            render={({ field: { onChange, ref, value } }) => (
              <Switch
                id="betterPrice"
                ref={ref}
                isChecked={value}
                onChange={(event) => onChange(event.target.checked)}
              />
            )}
          />
        </FormControl>

        <Button type="submit" variant="primary">
          {!quoteData ? 'Submit' : 'Update'}
        </Button>
      </form>
    </>
  );
};
