import { useState } from 'react';
import {
  Container,
  Table,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from '@chakra-ui/react';
import { FiEdit } from 'react-icons/fi';

import { QuoteData, getPreviousQuotes } from '../../utils';
import { QuoteEditModal } from './modals';

export const QuotesList = () => {
  const [editModal, setEditModal] = useState<Nullable<string>>(null);

  const tableBackgroundColor = useColorModeValue('#fff', 'default.light');

  const previousQuotes: QuoteData[] = getPreviousQuotes();

  const handleEditModalOpen = (
    name: string,
    event: React.MouseEvent<HTMLTableDataCellElement, MouseEvent>
  ) => {
    event.preventDefault();
    setEditModal(name);
  };

  const handleEditModalClose = () => {
    setEditModal(null);
  };

  return (
    <>
      <Container
        centerContent
        maxW="container.xl"
        pt={{ base: 16, md: 32 }}
        pb={16}
      >
        <Text alignSelf="flex-start" mb={5} fontSize="3xl">
          Your previously saved quotes:
        </Text>

        <TableContainer w="100%">
          <Table
            backgroundColor={tableBackgroundColor}
            borderRadius={5}
            boxShadow="xl"
          >
            <TableCaption>Your previously saved quotes</TableCaption>
            <Thead>
              <Tr>
                <Th color="default.dark">Name</Th>
                <Th color="default.dark">Age</Th>
                <Th color="default.dark">Gender</Th>
                <Th color="default.dark">Country</Th>
                <Th color="default.dark">Yearly distance</Th>
                <Th color="default.dark">Quote</Th>
                <Th color="default.dark"></Th>
              </Tr>
            </Thead>
            <Tbody>
              {previousQuotes?.map((quote) => (
                <Tr key={quote.name}>
                  <Td>{quote.name}</Td>
                  <Td>{quote.age}</Td>
                  <Td>{quote.gender}</Td>
                  <Td>{quote.country}</Td>
                  <Td>
                    {quote.yearlyDistance[0]}km - {quote.yearlyDistance[1]}km
                  </Td>
                  <Td>${quote.quote}</Td>
                  <Td
                    position="relative"
                    zIndex={1}
                    cursor="pointer"
                    onClick={(event) => handleEditModalOpen(quote.name, event)}
                  >
                    <FiEdit />
                  </Td>
                </Tr>
              ))}
            </Tbody>
            <Tfoot>
              <Tr>
                <Th color="default.dark">Name</Th>
                <Th color="default.dark">Age</Th>
                <Th color="default.dark">Gender</Th>
                <Th color="default.dark">Country</Th>
                <Th color="default.dark">Yearly distance</Th>
                <Th color="default.dark">Quote</Th>
                <Th color="default.dark"></Th>
              </Tr>
            </Tfoot>
          </Table>
        </TableContainer>
      </Container>

      {editModal ? (
        <QuoteEditModal
          allQuotesData={previousQuotes}
          editModal={editModal}
          onClose={handleEditModalClose}
        />
      ) : null}
    </>
  );
};
