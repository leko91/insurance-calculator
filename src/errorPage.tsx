import {
  isRouteErrorResponse,
  useNavigate,
  useRouteError,
} from 'react-router-dom';
import { Box, Button, Text } from '@chakra-ui/react';

export default function ErrorPage() {
  const navigate = useNavigate();
  const error = useRouteError();

  return (
    <Box
      w="100%"
      h="90vh"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      textAlign="center"
    >
      <Text as="h1">Oops!</Text>
      <Text fontSize="xl">Sorry, an unexpected error has occurred.</Text>

      {isRouteErrorResponse(error) && (
        <Text as="i" py={16} fontSize="3xl">
          {error.status} {error.statusText}
        </Text>
      )}

      <Button variant="link" fontSize="xl" onClick={() => navigate(-1)}>
        &larr; Go back
      </Button>
    </Box>
  );
}
